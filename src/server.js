const app = require('./app')

const port = process.env.PORT || 3000

app.set('port', port)

app.listen(port, function() {
    console.log('Server running in port', port)
})