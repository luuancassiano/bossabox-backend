const Tool = require('../models/Tool')
const Tag = require('../models/Tag')

class ToolTagsController {

    async index(req, res) {
        try {

            const tag = req.query.tag

            const tools = await Tool.findAll({
                attributes: ['id', 'title', 'link', 'description'],
                include: [
                    {
                        model: Tag,
                        as: 'tags',
                        where: {
                            title: tag
                        },
                        attributes: ['id', 'title'],
                        through: {
                            attributes: []
                        }
                    }
                ]
            })

            return res.status(200).json({
                result: tools
            })

        } catch (error) {
            return error.message
        }
    }
}

module.exports = new ToolTagsController()