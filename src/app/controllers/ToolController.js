const Tool = require('../models/Tool')
const Tag = require('../models/Tag')

class ToolController {

    async index(req, res) {
        try {

            const tools = await Tool.findAll({
                attributes: ['id', 'title', 'link', 'description'],
                include: [
                    {
                        model: Tag,
                        as: 'tags',
                        attributes: ['id', 'title'],
                        through: {
                            attributes: []
                        }
                    }
                ]
            })

            return res.status(200).json({
                result: tools
            })

        } catch (error) {
            return error.message
        }
    }

    async show(req, res) {
        try {

            const tool = await Tool.findByPk(req.params.id)

            return res.status(200).json({
                result: tool
            })

        } catch (error) {
            return error.message
        }
    }

    async store(req, res) {
        try {
            const { tags, ...data } = req.body

            const tool = await Tool.create(data)

            if(tags && tags.length > 0) {
                tool.setTags(tags)
            }

            return res.status(201).json({
                result: tool
            })

        } catch (error) {
            return error.message
        }
    }

    async update(req, res) {
        try {
            
            const tool = await Tool.findByPk(req.params.id)

            const newTool = await tool.update(req.body)

            return res.status(200).json({
                result: newTool
            })

        } catch (error) {
            return error.message
        }
    }

    async destroy(req, res) {
        try {
            
            const tool = await Tool.findByPk(req.params.id)

            await tool.destroy()

            return res.status(204).json({
                result: 'Tool deleted'
            })

        } catch (error) {
            return error.message
        }
    }
}

module.exports = new ToolController()