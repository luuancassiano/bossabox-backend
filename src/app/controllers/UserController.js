const User = require('../models/User')

class UserController {

    async store(req, res) {
        try {

            const userExists = await User.findOne({
                where: {
                    email: req.body.email
                }
            })

            if(userExists) {
                return res.status(400).json({
                    error: 'Este usuário já existe'
                })
            }

            const user = await User.create(req.body)

            return res.status(201).json({
                result: user
            })

        } catch (error) {
            return error.message
        }
    }
}

module.exports = new UserController()