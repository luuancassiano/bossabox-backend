const User = require('../models/User')

const jwt = require('jsonwebtoken')

const authConfig = require('../../config/auth')

class AuthController {

    async store(req, res) {
        try {
            const { email, password } = req.body

            const user = await User.findOne({
                where: {
                    email
                }
            })

            if(!user) {
                return res.status(400).json({
                    error: 'Erro ao tentar fazer login, verifique suas credênciais'
                })
            }

            if(!(await user.checkPassword(password))) {
                return res.status(400).json({
                    error: 'Erro ao tentar fazer login, verifique suas credênciais'
                })
            }

            const { id, fullname } = user

            return res.status(200).json({
                result: {
                    id,
                    fullname,
                    email
                },

                token: jwt.sign({ id }, authConfig.secret, {
                    expiresIn: authConfig.expiresIn
                })
            })

        } catch (error) {
            return error.message
        }
    }
}

module.exports = new AuthController()