const Tag = require('../models/Tag')

class TagController {

    async store(req, res) {
        try {
            const tag = await Tag.create(req.body)

            return res.status(201).json({
                result: tag
            })

        } catch (error) {
            return error.message
        }
    }
}

module.exports = new TagController()