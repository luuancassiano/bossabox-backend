const { Sequelize, Model } = require('sequelize')

class Tag extends Model {
    static init(sequelize) {
        super.init({
            title: Sequelize.STRING,
            description: Sequelize.STRING
        }, {
            sequelize
        })
    }

    static associate(models) {
        this.belongsToMany(models.Tool, {
            through: 'tools_tags',
            as: 'tools',
            foreignKey: 'tag_id'
        })
    }
}

module.exports = Tag