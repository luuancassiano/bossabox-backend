const { Sequelize, Model } = require('sequelize')

class Tool extends Model {
    static init(sequelize) {
        super.init({
            title: Sequelize.STRING,
            link: Sequelize.STRING,
            description: Sequelize.STRING
        }, {
            sequelize
        })
    }

    static associate(models) {
        this.belongsToMany(models.Tag, {
            through: 'tools_tags',
            as: 'tags',
            foreignKey: 'tool_id'
        })
    }
}

module.exports = Tool