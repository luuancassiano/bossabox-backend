const Sequelize = require('sequelize')

const databaseConfig = require('../config/database')

const User = require('../app/models/User')
const Tool = require('../app/models/Tool')
const Tag = require('../app/models/Tag')

const models = [User, Tool, Tag]

class Database {
    constructor() {
        this.connection = new Sequelize(databaseConfig)

        this.init()
        this.associate()
    }

    init() {
        models.map(model => model.init(this.connection))
    }

    associate() {
        models.map(model => {
            if(model.associate) {
                model.associate(this.connection.models)
            }
        })
    }
}

module.exports = new Database()