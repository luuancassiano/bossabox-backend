const express = require('express');
const routes = express.Router();

const AuthController = require('./app/controllers/AuthController')
const UserController = require('./app/controllers/UserController')
const TagController = require('./app/controllers/TagController')
const ToolController = require('./app/controllers/ToolController')
const ToolTagsController = require('./app/controllers/ToolTagsController')

const authMiddleware = require('./app/middlewares/auth')

routes.post('/api/auth', AuthController.store)

routes.post('/api/user', UserController.store)

routes.use(authMiddleware)

routes.post('/api/tag', TagController.store)

routes.get('/api/tools', ToolController.index)
routes.get('/api/tools/:id', ToolController.show)
routes.post('/api/tools', ToolController.store)

routes.get('/api/tag/tools', ToolTagsController.index)


module.exports = routes