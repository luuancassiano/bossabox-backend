require('./bootstrap')

const express = require('express')
const cors = require('cors')
const bodyParser = require('body-parser')

const routes = require('./routes')

require('./database')

class App {
    constructor() {
        this.server = express()

        this.middlewares()
        this.routes()
    }

    middlewares() {
        this.server.use(bodyParser.json())
        this.server.use(bodyParser.urlencoded({ extended: false }))
        this.server.use(cors())
    }

    routes() {
        this.server.use(routes)
    }
}

module.exports = new App().server