function soma(n1, n2) {
    return n1 + n2
}

test('if i call soma function with 1 and 2 it should return 3', () => {
    const result = soma(1, 2)

    expect(result).toBe(3)
})